import sqlite3
from sqlite3 import Error
import class_user


def initialize_db(db_file):
    con = None
    try:
        con = sqlite3.connect(db_file)
    except Error:
        print(Error)

    return con


def create_table(conn):
    create_sql = """CREATE TABLE IF NOT EXISTS players_scores (
                    id integer PRIMARY KEY,
                    login text,
                    password text,
                    score integer);"""
    try:
        c = conn.cursor()
        c.execute(create_sql)
    except Error as err:
        print(err)


def save_score(con, score_player):
    sql = '''INSERT INTO players_scores(login, score) VALUES (?, ?);'''
    cur = con.cursor()
    cur.execute(sql, score_player)
    con.commit()
    cur.close()


def login():
    print("Войти - 1, Регистрация - 2")
    q = int(input())
    if q == 1:
        user_login = input("Введите свой логин: ")
        user_password = input("Введите пароль: ")
        user = class_user.User(user_login, user_password)
        check_user(user.get_login(), user.get_password())
        return user.get_login()
    elif q == 2:
        reg()


def check_user(login_user, password_user, con=initialize_db("DB.db")):
    sql = f"SELECT login, password FROM players_scores WHERE login = '{login_user}' AND password = '{password_user}'"
    cur = con.cursor()
    cur.execute(sql)
    if cur.fetchone() is not None:
        print('Welcome')
        cur.close()
        return True
    else:
        print('Нет такого пользователя')
        login()
        cur.close()


def reg(con=initialize_db("DB.db")):
    r = True
    while r is True:
        user_login = input("Введите свой логин: ")
        user_password = input("Введите пароль: ")
        print("Идет регистрация..")
        cur = con.cursor()
        cur.execute(f"SELECT login, password FROM players_scores WHERE login = '{user_login}' AND password = '{user_password}'")
        check = cur.fetchone()
        if check is None:
            print("Идет регистрация...")
            data = (user_login, user_password)
            sql = """INSERT INTO players_scores 
            (login, password) 
            VALUES (?, ?);"""
            cur.execute(sql, data)
            con.commit()
            cur.close()
            print("Вы успешно зарегистрировались!")
            login()
            r = False
        else:
            print("Такой пользователь уже существует, попробуйте еще раз")