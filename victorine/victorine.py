import sqlite3
from sqlite3 import Error
import DBActive
import os.path
import class_user


def gen_dict():
    game_dct = {}
    with open("victorine_info.txt", "r") as f:
        for i in f:
            dcts = (i.split())
            game_dct[dcts[0]] = dcts[1]
    return game_dct


def game(dct_for_game, user_login):
    score_player = 0
    print("""Добро пожаловать на викторину угадай страну!
Правила: Вы будете получать название столицы. 
Если вы угадали, то получаете один балл.
Если не угадали - 0 баллов. 
В конце вы увидите свой результат. Удачи!
Если вы хотите завершить игру досрочно, введите q.
    """)
    for keys in dct_for_game:
        print(dct_for_game[keys].capitalize())
        choice = str(input("Введите ваш ответ: "))
        if choice.lower() == keys.lower():
            print("Правильно. +1 балл!")
            score_player += 1
        elif choice == 'q':
            break
        else:
            print("К сожалению, вы ошиблись :(")
    print(f'Ваш результат {score_player} из {len(dct_for_game)} баллов')
    menu()
    return user_login, score_player



def check_stat(con):
    sql = 'SELECT login, score FROM players_scores'
    cur = con.cursor()
    cur.execute(sql)
    result = cur.fetchall()
    for name, score in result:
        print(f'Пользователь: {name}')
        print(f'Результат: {score}')
    menu()


def update_victorine_info():
    question = 'y'
    while question == 'y':
        country = str(input("Введите название страны: "))
        capital = str(input("Введите столицу: "))
        with open('victorine_info.txt', 'a') as f:
            add = '\n' + country.lower() + ' ' + capital.lower()
            print(add, type(add))
            f.write(add)
        question = str(input("Если хотите добавить еще страну - нажмите y, все остальное - возврат в меню."))
    menu()


# def menu():
#     db = r'db.DB'
#     print("""Чтобы начать играть нажмите 1
# Для добавления новых стран нажмите 2
# Для просмотра статистики нажмите 3
# Все остальное - выход.""")
#     choose = int(input())
#     if choose == 1:
#         main()
#     elif choose == 2:
#         update_victorine_info()
#     elif choose == 3:
#         check_stat(DBActive.initialize_db(db))
#     else:
#         return 0


def main():
    user_login = DBActive.login()
    game_dct = gen_dict()
    db = r'db.DB'
    print("""Чтобы начать играть нажмите 1
    Для добавления новых стран нажмите 2
    Для просмотра статистики нажмите 3
    Все остальное - выход.""")
    choose = int(input())
    if choose == 1:
        score_player = game(game_dct, user_login)
    elif choose == 2:
        update_victorine_info()
    elif choose == 3:
        check_stat(DBActive.initialize_db(db))
    else:
        return 0
    conn = DBActive.initialize_db(db)
    if conn is not None:
        DBActive.create_table(conn)
        DBActive.save_score(conn, score_player)


# def welcome():
#     print("Чтобы войти нажмите 1, для регистрации нажмите 2")
#     check = int(input())
#     if check == 1:
#
#     elif check == 2:
#         DBActive.reg()
#     elif check == 3:
#         check_stat(DBActive.initialize_db("DB.db"))
#

if __name__ == "__main__":
    main()
