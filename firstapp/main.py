import tkinter as tk
from tkinter import *
from tkinter import ttk
import sys
from process import CpuToolsBar
from update_widget import ConfigWidgets


class mainWindow(tk.Tk, ConfigWidgets):

    def __init__(self):
        tk.Tk.__init__(self)
        self.attributes('-alpha', 1)
        self.attributes('-topmost', True)
        self.overrideredirect(False)
        self.resizable(False, False)
        self.title('MyFirstApp')
        self.cpu = CpuToolsBar()
        self.run_set_iu()
    def run_set_iu(self):

        self.set_ui()
        self.make_bar_cpu_usage()
        self.config_cpu_bar()


    def app_exit(self):
        self.destroy()
        sys.exit()

    def set_ui(self):

        self.bar2 = ttk.LabelFrame(self, text="Manual")
        self.bar2.pack(fill=tk.BOTH)
        self.tools_bar = ttk.Combobox(self.bar2,
                                      values=["hide", "don't hide", "min"],
                                      width=18, state="readonly")
        self.tools_bar.current(1)
        self.tools_bar.pack(side=tk.LEFT)
        ttk.Button(self.bar2, text="Replace", command=self.configure_win).pack(side=tk.LEFT)
        ttk.Button(self.bar2, text=">>>>").pack(side=tk.LEFT)
        self.bar3 = ttk.LabelFrame(self, text='Power')
        self.bar3.pack(fill=tk.BOTH)
        self.bind_class('Tk', '<Enter>', self.enter_mouse)
        self.bind_class('Tk', '<Leave>', self.leave_mouse)
        exit_button = ttk.Button(self, text="Выход", command=self.app_exit, underline=1)
        exit_button.pack(fill=tk.BOTH, padx=10)
        self.tools_bar.bind('<<ComboboxSelected>>', self.choice_min)

    def make_bar_cpu_usage(self):
        ttk.Label(self.bar3, text=f'physical cores: {self.cpu.cpu_count}, logical cores: {self.cpu.cpu_count_logical}',
                  anchor=tk.CENTER).pack(fill=tk.X)
        self.list_label = []
        self.list_progress_bar = []

        for i in range(self.cpu.cpu_count_logical):
            self.list_label.append(ttk.Label(self.bar3, anchor=tk.CENTER))
            self.list_progress_bar.append(ttk.Progressbar(self.bar3, length=100))
        for i in range(self.cpu.cpu_count_logical):
            self.list_label[i].pack(fill=tk.X)
            self.list_progress_bar[i].pack(fill=tk.X)
        self.ram_lab = ttk.Label(self.bar3, text='', anchor=tk.CENTER)
        self.ram_lab.pack(fill=tk.X)
        self.ram_bar = ttk.Progressbar(self.bar3, length=100)
        self.ram_bar.pack(fill=tk.X)

    def make_min_win(self):
        self.bar_min = ttk.Progressbar(self, length=100)
        self.bar_min.pack(side=tk.LEFT)
        self.ram_min = ttk.Progressbar(self, length=100)
        self.ram_min.pack(side=tk.LEFT)
        ttk.Button(self, text="full", width=7, command=self.show_full).pack(side=tk.RIGHT)
        ttk.Button(self, text="move", width=7, command=self.configure_win).pack(side=tk.RIGHT)
        self.update()
        self.config_min_size()

    def enter_mouse(self, event):
        if self.tools_bar.current() == 0 or 1:
            self.geometry('')

    def leave_mouse(self, event):
        if self.tools_bar.current() == 0:
            self.geometry(f'{self.winfo_width()}x1')

    def choice_min(self, event):
        if self.tools_bar.current() == 2:
            self.enter_mouse('')
            self.unbind_class('Tk', '<Enter>')
            self.unbind_class('Tk', '<Leave>')
            self.tools_bar.unbind('<<ComboboxSelected>>')
            self.after_cancel(self.wheel)
            self.clear_window()
            self.update()
            self.make_min_win()

    def show_full(self):
        self.after_cancel(self.wheel)
        self.clear_window()
        self.update()
        self.run_set_iu()
        self.enter_mouse('')
        self.tools_bar.current(1)


if __name__ == '__main__':
    root = mainWindow()
    root.mainloop()
