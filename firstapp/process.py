import psutil as pt


class CpuToolsBar:

    def __init__(self):
        self.cpu_count = pt.cpu_count(logical=False)
        self.cpu_count_logical = pt.cpu_count(logical=True)

    def cpu_percent_return(self):
        return pt.cpu_percent(percpu=True)

    def ram_usage(self):
        return pt.virtual_memory()

    def cpu_average(self):
        return pt.cpu_percent()

print(CpuToolsBar().ram_usage())