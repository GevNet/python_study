class ConfigWidgets:

    def config_cpu_bar(self):
        r = self.cpu.cpu_percent_return()
        for i in range(self.cpu.cpu_count_logical):
            self.list_label[i].configure(text=f'core {i+1} usage: {r[i]}%')
            self.list_progress_bar[i].configure(value=r[i])

        r2 = self.cpu.ram_usage()
        self.ram_lab.configure(text=f'RAM usage: {r2[2]}% used, {round(r2[1]/1048567)} MB, avalible: {round(r2[1]/1048567)}, MB')
        self.ram_bar.configure(value=r2[2])


        self.wheel = self.after(1000, self.config_cpu_bar)

    def configure_win(self):
        if self.overrideredirect():
            self.overrideredirect(False)
        else:
            self.overrideredirect(True)

    def clear_window(self):
        for i in self.winfo_children():
            i.destroy()

    def config_min_size(self):
        r1 = self.cpu.cpu_average()
        self.bar_min.configure(value=r1)
        self.ram_min.configure(value=self.cpu.ram_usage()[2])
        self.wheel = self.after(1000, self.config_min_size)

